import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ColorsDefaultSourceService} from './shared/colors-default-source.service';
import {PickerModule} from './picker/picker.module';
import { FormComponent } from './form/form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
  ],
  imports: [
    BrowserModule,
    PickerModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [
    ColorsDefaultSourceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
