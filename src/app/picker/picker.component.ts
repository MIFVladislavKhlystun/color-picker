import {Component, forwardRef, Input, OnDestroy, OnInit} from '@angular/core';
import {PickerService} from './picker.service';
import {TogglerService} from './toggler.service';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-picker',
  templateUrl: './picker.component.html',
  styleUrls: ['./picker.component.css'],
  providers: [
    PickerService,
    TogglerService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PickerComponent),
      multi: true
    }
  ]
})
export class PickerComponent implements OnInit, OnDestroy, ControlValueAccessor {

  @Input() colors: string[];

  subscription: Subscription;

  constructor(private pickerService: PickerService) {
  }

  ngOnInit() {
    this.subscription = this.pickerService.currentColor.subscribe(
      (color: string) => this.writeValue(color));
  }

  writeValue(value: string) {
    this.onChange(value);
    this.onTouched();
  }

  onChange = (val: string) => {};
  onTouched = () => {};

  registerOnChange(fn: () => void) {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void) {
    this.onTouched = fn;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

