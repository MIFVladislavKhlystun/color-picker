import { Component, OnInit } from '@angular/core';
import {TogglerService} from '../../toggler.service';

@Component({
  selector: 'app-picker-button',
  templateUrl: './picker-button.component.html',
  styleUrls: ['./picker-button.component.css']
})
export class PickerButtonComponent implements OnInit {

  constructor(public toggler: TogglerService) { }

  ngOnInit() {
  }

  toggleOpen() {
    this.toggler.toggleContainer();
  }

}
