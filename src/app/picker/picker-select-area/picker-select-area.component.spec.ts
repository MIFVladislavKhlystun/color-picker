import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickerSelectAreaComponent } from './picker-select-area.component';

describe('PickerSelectAreaComponent', () => {
  let component: PickerSelectAreaComponent;
  let fixture: ComponentFixture<PickerSelectAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickerSelectAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickerSelectAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
