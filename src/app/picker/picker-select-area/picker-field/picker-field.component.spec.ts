import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickerFieldComponent } from './picker-field.component';

describe('PickerFieldComponent', () => {
  let component: PickerFieldComponent;
  let fixture: ComponentFixture<PickerFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickerFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickerFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
