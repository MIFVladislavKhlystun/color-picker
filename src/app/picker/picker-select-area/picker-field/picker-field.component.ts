import {Component, OnDestroy, OnInit} from '@angular/core';
import {PickerService} from '../../picker.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-picker-field',
  templateUrl: './picker-field.component.html',
  styleUrls: ['./picker-field.component.css']
})
export class PickerFieldComponent implements OnInit, OnDestroy {

  selectedColor: string;
  selectedColorSubscription: Subscription;

  constructor(private picker: PickerService) {
  }

  ngOnInit() {
    this.selectedColorSubscription = this.picker.currentColor.subscribe(
      (color: string) => this.selectedColor = color
    );
  }

  ngOnDestroy() {
    this.selectedColorSubscription.unsubscribe();
  }

}
