import {Directive, HostListener} from '@angular/core';
import {TogglerService} from './toggler.service';

@Directive({
  selector: '[appKeyboardHandler]'
})
export class KeyboardHandlerDirective {

  static getColumnsNumber() {
    const style = getComputedStyle(document.querySelector('.colors-container'));
    return style.gridTemplateColumns.split(' ').length;
  }

  static onPressArrow(e: KeyboardEvent) {
    const arrows = ['ArrowRight', 'ArrowUp', 'ArrowLeft', 'ArrowDown'];
    if (arrows.indexOf(e.code) === -1) {
      return;
    }
    const columnsNumber = KeyboardHandlerDirective.getColumnsNumber();
    const items = document.querySelectorAll('.color-item');
    let elementIndex: number = (e.target as HTMLElement).tabIndex - 1;

    switch (e.code) {
      case 'ArrowRight':
        elementIndex += 1;
        break;
      case 'ArrowLeft':
        elementIndex -= 1;
        break;
      case 'ArrowUp':
        elementIndex -= columnsNumber;
        break;
      case 'ArrowDown':
        elementIndex += columnsNumber;
        break;
    }

    if (elementIndex >= items.length || elementIndex < 0) {
      return;
    }

    (items[elementIndex] as HTMLElement).focus();

    // set in service selected color?
  }

  @HostListener('keyup', ['$event']) onPress(e: KeyboardEvent) {
    if (e.code === 'Escape' || e.code === 'Enter') {
      this.toggler.isOpened.next(false);
    }
    KeyboardHandlerDirective.onPressArrow(e);
  }

  constructor(private toggler: TogglerService) {
  }


}
