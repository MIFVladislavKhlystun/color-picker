import {Component, ElementRef, Input, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {Subscription} from 'rxjs';
import {TogglerService} from '../toggler.service';
import {ColorsDefaultSourceService} from '../../shared/colors-default-source.service';
import {PickerService} from '../picker.service';
import {nextTick} from 'q';

@Component({
  selector: 'app-picker-container',
  templateUrl: './picker-container.component.html',
  styleUrls: ['./picker-container.component.css']
})
export class PickerContainerComponent implements OnInit, OnDestroy {

  @ViewChildren('colorItem') colorItems: QueryList<ElementRef>;
  @Input() receivedColors: string[];

  isOpened = false;
  colors = [];
  subscription: Subscription;

  constructor(private toggler: TogglerService,
              private defaultColorsStorage: ColorsDefaultSourceService,
              private picker: PickerService) {
  }

  ngOnInit() {
    if (this.receivedColors && this.receivedColors.length) {
      this.colors = this.receivedColors;
    } else {
      this.colors = this.defaultColorsStorage.colorsList;
    }
    this.subscription = this.toggler.isOpened.subscribe(
      (state: boolean) => {
        this.isOpened = state;
        if (state) {
          nextTick(() => this.focusColorOnOpen(this.colorItems));
          // setTimeout(() => this.focusColorOnOpen(), 0);
        }
      }
    );
  }

  focusColorOnOpen(items: QueryList<ElementRef>) {
    if (this.isOpened && this.picker.selectedColor) {
      items.forEach((el: ElementRef) => {
        if (this.picker.selectedColor === el.nativeElement.style.backgroundColor) {
          el.nativeElement.focus();
          return;
        }
      });
    }
  }

  onChooseColor(color: string) {
    this.picker.setCurrentColor(color);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
