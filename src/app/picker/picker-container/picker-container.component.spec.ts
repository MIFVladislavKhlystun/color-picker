import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickerContainerComponent } from './picker-container.component';

describe('PickerContainerComponent', () => {
  let component: PickerContainerComponent;
  let fixture: ComponentFixture<PickerContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickerContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickerContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
