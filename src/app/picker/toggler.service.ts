import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class TogglerService {
  // isOpened = new EventEmitter<void>();
  isOpened = new BehaviorSubject<boolean>(false);

  get openFlag() {
    return this.isOpened.getValue();
  }

  set openFlag(value: boolean) {
    this.isOpened.next(value);
  }

  constructor() { }

  toggleContainer() {
    this.isOpened.next(!this.isOpened.getValue());
  }
}
