import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class PickerService {
  currentColor = new Subject<string>();
  selectedColor = '';

  constructor() {
  }

  setCurrentColor(color: string) {
    this.currentColor.next(color);
    this.selectedColor = color;
  }
}
