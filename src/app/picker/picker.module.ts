import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PickerContainerComponent} from './picker-container/picker-container.component';
import {PickerSelectAreaComponent} from './picker-select-area/picker-select-area.component';
import {PickerButtonComponent} from './picker-select-area/picker-button/picker-button.component';
import {PickerFieldComponent} from './picker-select-area/picker-field/picker-field.component';
import {CloseDirective} from './close.directive';
import { PickerComponent } from './picker.component';
import {BrowserModule} from '@angular/platform-browser';
import { KeyboardHandlerDirective } from './keyboard-handler.directive';

@NgModule({
  declarations: [
    PickerContainerComponent,
    PickerSelectAreaComponent,
    PickerButtonComponent,
    PickerFieldComponent,
    CloseDirective,
    PickerComponent,
    KeyboardHandlerDirective
  ],
  imports: [
    BrowserModule,
    CommonModule
  ],
  exports: [
    PickerComponent
  ],
  providers: []
})
export class PickerModule {}
