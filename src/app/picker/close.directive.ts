import {Directive, ElementRef, HostListener} from '@angular/core';
import {TogglerService} from './toggler.service';

@Directive({
  selector: '[appClose]'
})
export class CloseDirective {
  constructor(private elementRef: ElementRef,
              private toggler: TogglerService) { }

  @HostListener('document:click', ['$event'])
  onClick(event: MouseEvent) {
    const targetElement = event.target as HTMLElement;
    if (targetElement.classList.contains('picker') || targetElement.classList.contains('caret')) { return; }
    if (targetElement && !this.elementRef.nativeElement.contains(targetElement)) {
      this.toggler.toggleContainer();
    }
  }
}
