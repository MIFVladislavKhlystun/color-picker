import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorsDefaultSourceService {

  private readonly colors = [
    'red',
    'orange',
    'yellow',
    'green',
    'aqua',
    'blue',
    'rebeccapurple',
    'silver',
  ];

  constructor() { }

  get colorsList() {
    return this.colors;
  }
}
